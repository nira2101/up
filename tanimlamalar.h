/*
 * tanimlamalar.h
 *
 *  Created on: 13 Mar 2021
 *      Author: User
 */

#ifndef TANIMLAMALAR_H_
#define TANIMLAMALAR_H_

struct SteeringType
{
    byte speedMode;
    byte positionMode;
    byte speedEnable;
    byte positionEnable;
    byte writenposEnableStatus;
    int writenPosValue;
    byte writePosComplete;
    byte posEnableState;
    int speed;
    int position;
    int mesafemm;
    byte queryMessage;
    byte sorguState;
    byte toggleMesafe;
    float dumenAciDegeri;
    float dumenDirenc;
    float maDumenDirenc;
    float maMotorPos;
    float dumenofset;
    float maxlimit;
    float minlimit;

};
struct SteeringType steeringVar;
//struct SteeringType inSteering;

struct SubSystemTypeBits
{
    byte gpsBazIstasyonu:1;
    byte gpsNTRIP:1;
    byte gpsUydu:1;
    byte Incilometer:1;

    byte bno055:1;
    byte reserved1:1;
    byte dumenAciOlcer:1;
    byte reserved2:1;

    byte elektrikliDumen:1;
    byte hidrolikDumen:1;
    byte reserved5:1;
    byte reserved6:1;

    byte reserved7:1;
    byte reserved8:1;
    byte reserved9:1;
    byte reserved10:1;
};

union SubSystemTypes
{
    unsigned short bayt;
    struct SubSystemTypeBits bit;
};
SubSystemTypes altsistemler;

enum enableDisableEnum
{
    DISABLE = 0,
    ENABLE= 1,
};










byte otaClosed = 1;
byte potaClosed = 1;
byte ilkAcilisForwifi = 1;



//debug
short tempampsdata;
#endif /* TANIMLAMALAR_H_ */
