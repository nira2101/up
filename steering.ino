/*
 * NOTLAR : Degisken ve strucy yapisi kontrol edilecek
 *
 */

#include "tanimlamalar.h"

#define SteeringPRINT
void DumenlemeYonet(void);
void DumenlemeIceriginiDoldur(byte *veri);
void ElektrikDumeniIlklendir();
void dmVoltajKontrolu();
void dmAkimKontrolu();
void dmPozisyonKontrolEt();
void dmEnableDisableYonet();
void SorguSiralamasiniYonet();
void CalcFilteredPosition();
//struct SteeringType
//{
//    byte speedMode;
//    byte positionMode;
//    byte speedEnable;
//    byte positionEnable;
//    int speed;
//    int position;
//    byte queryMessage;
//
//};
//struct SteeringType steeringVar;
////struct SteeringType inSteering;
//  typedef enum SensorDurumu
//  {
//      _ILKLENDIRME = 0,
//      _NORMAL= 1,
//      _KONUMYOK= 2,
//      _ARIZALI = 3,
//      _NOTAVALIABLE = 4,
//      _KONUMVAR = 5
//  }SensorDurumTipi;

struct dmStatusTypes
{
    byte voltajDurumu;
    byte akimDurumu;
    int pos;
};
dmStatusTypes dmStatus;
enum dmQueyNumber
{
    ElectricAngle = 1,
    Speed = 2,
    Current = 3,
    RotorMachanicalPosition = 4,
    Voltage = 5,
    Temperature = 6,
    ErrorCode =7,
    Position = 8,
    ProgramVersion = 9,
};
enum manOtoEnum
{
    MANUEL = 1,
    OTOMATIK = 2,
};
enum sorgulamaStateEnum
{
    OFSETSORGULAMA = 0,
    SIRALISORGULAMA = 1,
};

byte motorControl = 0;
int commandQuery = 1;


byte mga[100];
byte mgaTemp[100];
int gelenVeriCnt = 0;

struct IncomingDataTypes
{
    byte checkResult;
    short ErroCode;
    short SpeedData;
    short AmpsData;
    short checkCodeSum;
    bool Gecerlilik;
};
IncomingDataTypes incoming, tempIncoming;

struct ErrorDataTypeBits
{
    byte workingNormaly:1;
    byte overVoltage:1;
    byte overCurrent:1;
    byte eepromError:1;

    byte lessVoltage:1;
    byte brake:1;
    byte overCurrentProtection:1;
    byte controlModeFailure:1;

    byte workingModeFailure:1;
    byte speedLossProtection:1;
    byte temperatureAlarm:1;
    byte hallError:1;

    byte Reversed:1;
    byte break232:1;
    byte canBreak:1;
    byte blockingforTwoSeconds:1;
};
union ErrorTypes
{
    unsigned short bayt;
    struct ErrorDataTypeBits bit;
};
struct ControlStatusTypeBits
{
    byte reversed:4;
    byte mode:4;
    byte commType:4;
    byte encoderType:4;

};
union ControlStatusTypes
{
    unsigned short bayt;
    struct ControlStatusTypeBits bit;
};
struct QueryDataTypes
{
    byte MotorSpeed;
    byte controllerVoltage;
    byte current;
    short temperature;
    unsigned short electricalAngle;
    unsigned short mecRotorPosition;
    int MotorPosition;
    int progVersion;
    ControlStatusTypes ControlStatus;
    ErrorTypes ErrorCode;
    //ErrorTypes subError;
};
QueryDataTypes pqueryData, queryData, tempQueryData;
struct SteeringZamanTypes
{
    bool queryGecerlilik[10];
    bool queryZamanAsimi[10];
    unsigned long int zaman[10];
    bool posZamanAsimi;
    unsigned long int posZaman;
};
SteeringZamanTypes steerZaman;
struct PozisyonTypes
{
    float calcPosValue;/*Aci sensorunden okunan ve pos cevrilen deger*/
    float maCalcPosValue;
    float sensorAciDegeri;
    float maSensorAciDegeri;
    float avePosValue; /*Motordan sorgulalan deger*/
    float aveCalcPosValue;
    int offset;
    int manueltoOtopos;
    int filteredpos;
};
PozisyonTypes dumenPos;
//struct SteeringType
//{
//    byte sorguState;
//
//};
//struct SteeringType steer;
// Zaman Sayaclari
unsigned long int manuelOtoGecisZS = 0;

void MotorKontrolVerisiOlusturGonder(void)
{
    /*
     * speedMode, PositionMode, speedEnable, positionEnable, Speed, Position
     * */

    byte sendArray[20];
    byte convert4Array[4];
    memset(sendArray,0,sizeof(sendArray));

    if(motorControl == 1)
    {
        sendArray[0] = 0xAC;
        sendArray[1] = 0XEC;
        if(steeringVar.speedMode == 1)
        {
            if (steeringVar.speedEnable == 1)
                sendArray[2] = 0x11;
            else
                sendArray[2] = 0x10;
            memcpy(&convert4Array[0], &steeringVar.speed, sizeof(steeringVar.speed));
            //memcpy(&sendArray[3], &steeringVar.speed, sizeof(steeringVar.speed));
            sendArray[3] = convert4Array [3];
            sendArray[4] = convert4Array [2];
            sendArray[5] = convert4Array [1];
            sendArray[6] = convert4Array [0];
            /* int speedData = -550;
            byte[] baytConvert = BitConverter.GetBytes(rpmValue);
            sendArray[3] = baytConvert[3];
            sendArray[4] = baytConvert[2];
            sendArray[5] = baytConvert[1];
            sendArray[6] = baytConvert[0];*/
        }
        else if (steeringVar.positionMode == 1)
        {
            //#ifdef SteeringPRINT
            //            Serial.println("writenposEnableStatus : ");
            //            Serial.println(steeringVar.writenposEnableStatus);
            //#endif

            memcpy(&convert4Array[0], &steeringVar.writenPosValue, sizeof(steeringVar.writenPosValue));

            Serial.print("writenPosValue : ");
            Serial.println(steeringVar.writenPosValue);
            if (steeringVar.writenposEnableStatus == 1)
            {
                sendArray[2] = 0x21;
                steeringVar.writePosComplete = 1;
                steeringVar.writenPosValue =0;
            }
            else
                sendArray[2] = 0x20;
            //memcpy(&sendArray[3], &steeringVar.position, sizeof(steeringVar.position));


            sendArray[3] = convert4Array [3];
            sendArray[4] = convert4Array [2];
            sendArray[5] = convert4Array [1];
            sendArray[6] = convert4Array [0];

        }
    }
    else
    {
        sendArray[0] = 0xED;
        sendArray[1] =  steeringVar.queryMessage;
    }
    int checksum = 0, cl = 0;
    for (int i = 0; i < 7; i++)
    {
        checksum += sendArray[i];
    }
    cl = checksum % 256;
    if(motorControl ==1)
    {
        sendArray[7] = (byte)cl;
    }
    motorSerial.write(sendArray, 8);

}
void MotorGelenVeriOku()
{

    byte convert4Array[4];
    memset(&mga[0],0,100);
    gelenVeriCnt = 0;
    if(motorSerial.available()>10)
    {
        while(motorSerial.available())
        {
            char c = motorSerial.read();

#ifdef SteeringPRINT
            //            Serial.print(c,HEX);
            //            Serial.print(",");
#endif
            if(gelenVeriCnt <100)
                mga[gelenVeriCnt++] = c;

        }

    }

#ifdef SteeringPRINT
    //    Serial.println("");
#endif
    int tempGelenVeriCnt = 0;
    if(gelenVeriCnt>=6)
    {
        for(int i=0;i<gelenVeriCnt-1;i++)
        {
            tempGelenVeriCnt++;

            if(mga[i]==0xAC )
            {

                //Serial.println("0xac data ");
                if(mga[i+1]==0xEE)
                {
                    //Serial.println("0xee data ");
                    if(gelenVeriCnt-i>=10)
                    {

                        //Serial.println("boyut data ");
                        int checksum = 0;
                        for(int c = i;c<(i+9);c++)
                        {
                            checksum += mga[c];
                        }
                        //Serial.print("checksum : ");
                        //Serial.println(checksum);
                        tempIncoming.checkResult = mga[i + 2];
                        short shortVar=0;
                        memcpy(&shortVar, &mga[i+3], sizeof(shortVar));
                        tempIncoming.ErroCode = shortVar;
                        memcpy(&shortVar, &mga[i+5], sizeof(shortVar));
                        tempIncoming.SpeedData = shortVar;
                        memcpy(&shortVar, &mga[i+7], sizeof(shortVar));
                        tempIncoming.AmpsData = shortVar;
                        memcpy(&shortVar, &mga[i+9], sizeof(shortVar));
                        tempIncoming.checkCodeSum = shortVar;

                        //Serial.print("checkCodeSum : ");
                        //Serial.println(tempIncoming.checkCodeSum);

                        if(checksum == tempIncoming.checkCodeSum)
                        {
                            i=i+11;
                            incoming = tempIncoming;
                            incoming.Gecerlilik = true;
                            //#ifdef SteeringPRINT
                            //                            Serial.print("checkResult : " );
                            //                            Serial.println(tempIncoming.checkResult);
                            //                            Serial.print("ErroCode : ");
                            //                            Serial.println(tempIncoming.ErroCode);
                            //                            Serial.print("SpeedData : ");
                            //                            Serial.println(tempIncoming.SpeedData);
                            //                            Serial.print("AmpsData : ");
                            //                            Serial.println(tempIncoming.AmpsData);
                            //#endif
                        }
                        else
                        {
                            incoming.Gecerlilik = false;
                        }
                    }
                }
            }
            else if(mga[i]==0xED)
            {
                if (gelenVeriCnt - i >= 6)
                {
                    int checksum = 0;
                    for (int c = 1; c < 7; c++)
                    {
                        checksum += mga[c];
                    }

                    short shortVar=0;
                    short uShortVar=0;
                    int intVar=0;
                    short checkCodeSum=0;
                    memcpy(&checkCodeSum, &mga[i+7], sizeof(checkCodeSum));

                    //if (checksum == tempIncoming.checkCodeSum)
                    {
                        if (mga[i + 1] < 10)
                        {

                            steerZaman.queryZamanAsimi[mga[i + 1]] = true;
                            steerZaman.queryGecerlilik[mga[i+1]] = true;
                            steerZaman.zaman[mga[i+1]] = millis();
                            switch (mga[i + 1])
                            {
                                case 1:
                                    convert4Array[0] = mga[i+3];
                                    convert4Array[1] = mga[i+2];
                                    memcpy(&uShortVar, &convert4Array[0], sizeof(uShortVar));
                                    tempQueryData.electricalAngle = uShortVar;
                                    //#ifdef SteeringPRINT
                                    //                                    Serial.print("electricalAngle : ");
                                    //                                    Serial.println(tempQueryData.electricalAngle);
                                    //#endif
                                    break;
                                case 2:
                                    tempQueryData.MotorSpeed = mga[i + 3];
                                    //#ifdef SteeringPRINT
                                    //                                    Serial.print("MotorSpeed : " );
                                    //                                    Serial.println(tempQueryData.MotorSpeed);
                                    //#endif
                                    break;
                                case 3:
                                    tempQueryData.current = mga[i + 3];
                                    //#ifdef SteeringPRINT
                                    //                                    Serial.print("controllerCurrent : ");
                                    //                                    Serial.println(tempQueryData.current);
                                    //#endif
                                    break;
                                case 4:
                                    convert4Array[0] = mga[i+3];
                                    convert4Array[1] = mga[i+2];
                                    memcpy(&uShortVar, &convert4Array[0], sizeof(uShortVar));
                                    //memcpy(&uShortVar, &mga[i+2], sizeof(uShortVar));
                                    tempQueryData.mecRotorPosition = uShortVar;
                                    //#ifdef SteeringPRINT
                                    //                                    Serial.print("mecRotorPosition : ");
                                    //                                    Serial.println(tempQueryData.mecRotorPosition);
                                    //#endif
                                    break;
                                case 5:
                                    tempQueryData.controllerVoltage = mga[i + 2];
                                    //#ifdef SteeringPRINT
                                    //                                    Serial.print("controllerVoltage : ");
                                    //                                    Serial.println(tempQueryData.controllerVoltage);
                                    //#endif
                                    break;
                                case 6:
                                    tempQueryData.temperature = mga[i + 3];
                                    //#ifdef SteeringPRINT
                                    //                                    Serial.print("temperature : " );
                                    //                                    Serial.println( tempQueryData.temperature);
                                    //#endif
                                    break;
                                case 7:
                                    convert4Array[0] = mga[i+3];
                                    convert4Array[1] = mga[i+2];
                                    memcpy(&uShortVar, &convert4Array[0], sizeof(uShortVar));
                                    //memcpy(&shortVar, &mga[i+2], sizeof(shortVar));
                                    tempQueryData.ErrorCode.bayt = shortVar;
                                    //#ifdef SteeringPRINT
                                    //                                    Serial.print("ErrorCode : ");
                                    //                                    Serial.println( tempQueryData.ErrorCode.bayt);
                                    //#endif
                                    //tempQueryData. subError = tempQueryData.ErrorCode;
                                    break;
                                case 8:
                                    //Serial.println("Motor Position");
                                    convert4Array[0] = mga[i+5];
                                    convert4Array[1] = mga[i+4];
                                    convert4Array[2] = mga[i+3];
                                    convert4Array[3] = mga[i+2];
                                    steerZaman.posZaman = millis();
                                    memcpy(&intVar, &convert4Array[0], sizeof(intVar));
                                    //memcpy(&intVar, &mga[i+2], sizeof(intVar));
                                    tempQueryData.MotorPosition = intVar;
                                    //#ifdef SteeringPRINT
                                    //                                    Serial.print("MotorPosition : ");
                                    //                                    Serial.println(tempQueryData.MotorPosition);
                                    //#endif
                                    break;
                                case 9:
                                    memcpy(&intVar, &mga[i+2], sizeof(intVar));
                                    tempQueryData.progVersion = intVar;
                                    //#ifdef SteeringPRINT
                                    //                                    Serial.print("progVersion : ");
                                    //                                    Serial.println(tempQueryData.progVersion);
                                    //#endif
                                    break;
                                case 0:
                                    convert4Array[0] = mga[i+3];
                                    convert4Array[1] = mga[i+2];
                                    memcpy(&uShortVar, &convert4Array[0], sizeof(uShortVar));
                                    tempQueryData.ControlStatus.bayt = uShortVar;
                                    //#ifdef SteeringPRINT
                                    //                                    Serial.println("ControlStatus : " );
                                    //                                    Serial.println(tempQueryData.ControlStatus.bayt);
                                    //                                    Serial.println(tempQueryData.ControlStatus.bit.encoderType);
                                    //                                    Serial.println(tempQueryData.ControlStatus.bit.commType);
                                    //                                    Serial.println(tempQueryData.ControlStatus.bit.mode);
                                    //#endif
                                    break;
                                default:
                                    break;
                            }
                            queryData = tempQueryData;

                            i=i+5;
                        }
                    }
                }
            }

        }
    }
    //gelenVeriCnt = gelenVeriCnt - tempGelenVeriCnt;

    //memcpy(&shortVar, &mga[i+2], sizeof(shortVar));

    //#ifdef SteeringPRINT
    //    Serial.println("end of data ");
    //#endif
}
void DumenlemeYonet(void)
{

    /*Seri Veri Oku*/
    MotorGelenVeriOku();
    dmPozisyonKontrolEt();
    /*Verileri Decode Et*/

    // Sorgulari Yonet
    if(steeringVar.sorguState == OFSETSORGULAMA)
    {
        static unsigned int posCnt = 0;
        motorControl = 0;
        steeringVar.queryMessage = Position;

        steeringVar.writenposEnableStatus = 0;
        steeringVar.speedMode = 0;
        steeringVar.positionMode = 1;
        //steeringVar.position = 0;

        if(steerZaman.posZamanAsimi && steerZaman.queryGecerlilik[Position] == true)
        {
            //#ifdef SteeringPRINT
            Serial.print("OFSETSORGULAMA Cnt++");
            Serial.print(posCnt);
            Serial.println("  ,  ");
            //#endif
            posCnt++;
            dumenPos.avePosValue = (dumenPos.avePosValue*(posCnt-1) + (float)queryData.MotorPosition)/posCnt;
            dumenPos.aveCalcPosValue = (dumenPos.aveCalcPosValue*(posCnt-1) + dumenPos.calcPosValue)/posCnt;
        }
        else
        {

            Serial.print("ofset cnt ");
            Serial.print(steerZaman.posZamanAsimi);
            Serial.print(",");
            Serial.print(steerZaman.queryGecerlilik[Position]);
            posCnt = 0;
        }
        if(posCnt>50)
        {
            /* 50 sample deger alindi
             * offset sifirlama islemi yapilacak*/

            if(dumenPos.aveCalcPosValue > 20000)
            {
                dumenPos.aveCalcPosValue = 20000;
            }
            else if((int)dumenPos.aveCalcPosValue < -20000)
            {
                dumenPos.aveCalcPosValue = -20000;
            }
            if(altsistemler.bit.dumenAciOlcer == ENABLE)
            {
                dumenPos.offset = dumenPos.avePosValue - (int)dumenPos.aveCalcPosValue;
            }
            else
            {
                dumenPos.offset = 0;
            }
            dumenPos.manueltoOtopos = queryData.MotorPosition;
            dumenPos.filteredpos = 0;
            steeringVar.sorguState = SIRALISORGULAMA;
            posCnt = 0;
            Serial.println("dumenPos.avePosValue: ");
            Serial.println(dumenPos.avePosValue);
            Serial.println("dumenPos.aveCalcPosValue: ");
            Serial.println(dumenPos.aveCalcPosValue);
            Serial.println("dumenPos.offset: ");
            Serial.println(dumenPos.offset);
        }
        //#ifdef SteeringPRINT
        //        Serial.println("OFSETSORGULAMA");
        //#endif

    }
    else if(steeringVar.sorguState == SIRALISORGULAMA)
    {
        SorguSiralamasiniYonet();
        dmEnableDisableYonet();
        //#ifdef SteeringPRINT
        //        Serial.println("SIRALISORGULAMA");
        //#endif
        CalcFilteredPosition();
    }



    unsigned long int zaman = millis();
    for (int c = 0; c < 10; c++)
    {
        if((zaman - steerZaman.zaman[c]) > 5000)
        {
            steerZaman.queryZamanAsimi[c] = false;
        }
        else
        {
            steerZaman.queryZamanAsimi[c] = true;
        }
    }

    // Motor Calisiyormu Kontrol Et
    if((zaman-steerZaman.posZaman) > 2000)
    {
        // TODO Zaman asimi hatasi uret
        steerZaman.posZamanAsimi = false;
        steeringVar.sorguState = OFSETSORGULAMA;
        dumenPos.avePosValue = 0;
        dumenPos.aveCalcPosValue = 0;

        for (int c = 0; c < 10; c++)
        {
            steerZaman.queryZamanAsimi[c] = false;
            steerZaman.queryGecerlilik[c] = false;

        }
    }
    else
    {
        steerZaman.posZamanAsimi = true;
    }

    //status Verilerini Kontrol et
    dmVoltajKontrolu();
    dmAkimKontrolu();


    /*Seri Veri Yaz*/
    MotorKontrolVerisiOlusturGonder();


    // DEBUG print
#ifdef SteeringPRINT
    if(queryData.current != pqueryData.current)
    {
        Serial.print("controllerCurrent : ");
        Serial.println(queryData.current);
    }

    if(queryData.controllerVoltage != pqueryData.controllerVoltage)
    {
        Serial.print("controllerVoltage : ");
        Serial.println(queryData.controllerVoltage);
    }

    if(queryData.temperature != pqueryData.temperature)
    {
        Serial.print("temperature : " );
        Serial.println( queryData.temperature);
    }


    if(queryData.ErrorCode.bayt != pqueryData.ErrorCode.bayt)
    {
        Serial.print("ErrorCode : ");
        Serial.println( queryData.ErrorCode.bayt);
    }

    if(abs(queryData.MotorPosition - pqueryData.MotorPosition) >200)
    {
        Serial.print("MotorPosition : ");
        Serial.println(queryData.MotorPosition);
    }

    if(tempampsdata != incoming.AmpsData)
    {
        Serial.print("AmpsData : ");
        Serial.println(incoming.AmpsData);
    }

#endif
    tempampsdata = incoming.AmpsData;
    pqueryData=queryData;

}

void dmVoltajKontrolu()
{
    if(steerZaman.queryZamanAsimi[Voltage] == true)
    {
        if(queryData.controllerVoltage == 0)
        {
            dmStatus.voltajDurumu = 0;
        }
        else if(queryData.controllerVoltage <7)
        {
            dmStatus.voltajDurumu = 2;
        }
        else if(queryData.controllerVoltage >30)
        {
            dmStatus.voltajDurumu = 3;
        }
        else if(queryData.controllerVoltage >=7 && queryData.controllerVoltage <=30)
        {
            dmStatus.voltajDurumu = 1;
        }
        else
        {
            dmStatus.voltajDurumu = 4;
        }
    }
}
void dmAkimKontrolu()
{
    //    if(steerZaman.Gecerlilik[Current] == true)
    //    {
    //        if(queryData.current >=3)
    //        {
    //            // Over Current
    //            dmStatus.akim = 2;
    //        }
    //        else
    //        {
    //            // Normal Operation
    //            dmStatus.akimDurumu = 1;
    //        }
    //    }

    if(incoming.Gecerlilik == true)
    {
        if(incoming.AmpsData>=3)
        {
            // Over Current
            dmStatus.akimDurumu = 2;
        }
        else
        {
            // Normal Operation
            dmStatus.akimDurumu = 1;
        }
    }


}
void dmPozisyonKontrolEt()
{
    /* aci sensorunden gelen veriyi oku ve direksiyon acisini pozisyonunu hesapla
    1-) Anlik aci degerinden pozisyonu hesapla
    2-) Moving Average sonrasi aci degeri hesapla

    end-)burada bulunan degeri yazilan pozisyon degeri ile karsilastiirilacak.

    aci sensoru ofset 1.65 VDC
                     tam sag icin 0 VDC
                     tam sol icin 3.3 VDc verdigi durum icin hesaplamalar yapilmistir.

     Direksiyon pozisyon degeri -20000 / 0 / +20000 olarak hesaplanacaktır.
     */
    // Analog Kanal'dan voltaj degerini oku

    /*
     * Motor pozisyonu ve direnc degeri arasinda ki iliskiyi cikar
     * 1-) oncelikler direnc ve yon arasindaki iliski
     * 2-) max ve min noktalari icin limitleri bul
     *
     */


    if(steeringVar.dumenDirenc <450 && steerZaman.queryGecerlilik[Position] == true)
    {
        steeringVar.maDumenDirenc = 0.25*steeringVar.dumenDirenc + 0.75*steeringVar.maDumenDirenc;
        steeringVar.maMotorPos = 0.25*queryData.MotorPosition + 0.75*steeringVar.maMotorPos;

        float dumenofset = 0;
        if(steeringVar.maDumenDirenc < 198 && steeringVar.maDumenDirenc > 182)
        {
            float direnctoPos = 0;
            steeringVar.minlimit = -14000;
            steeringVar.maxlimit = 10000;
            dumenofset = direnctoPos;
        }
        else if(steeringVar.maDumenDirenc < 190)
        {
            float direnctoPos = (190-steeringVar.maDumenDirenc)*90;
//#ifdef LABTEST
            steeringVar.minlimit = -14000;// direnctoPos - 17000;
            steeringVar.maxlimit = 10000; //direnctoPos + 12000;
//#else
//            steeringVar.minlimit = direnctoPos - 17000;
//            steeringVar.maxlimit = direnctoPos + 12000;
//
//#endif
            dumenofset = -1*direnctoPos;
        }
        else
        {
            float direnctoPos = (steeringVar.maDumenDirenc-190)*-65;
            steeringVar.minlimit = -14000; // direnctoPos - 17000;
            steeringVar.maxlimit = 10000; // direnctoPos + 12000;
            dumenofset = -1*direnctoPos;

        }

        /*
    if(steeringVar.dumenDirenc <450 && steerZaman.queryGecerlilik[Position] == true)
    {
        steeringVar.maDumenDirenc = 0.25*steeringVar.dumenDirenc + 0.75*steeringVar.maDumenDirenc;
        steeringVar.maMotorPos = 0.25*queryData.MotorPosition + 0.75*steeringVar.maMotorPos;

        float dumenofset = 0;
        if(steeringVar.maDumenDirenc < 198 && steeringVar.maDumenDirenc > 182)
        {
            float direnctoPos = 0;
            steeringVar.minlimit = steeringVar.maMotorPos -direnctoPos - 16000;
            steeringVar.maxlimit = steeringVar.maMotorPos -direnctoPos + 11000;
            dumenofset = direnctoPos;
        }
        else if(steeringVar.maDumenDirenc < 190)
        {
            float direnctoPos = (190-steeringVar.maDumenDirenc)*-90;
            steeringVar.minlimit = steeringVar.maMotorPos -direnctoPos - 17000;
            steeringVar.maxlimit = steeringVar.maMotorPos -direnctoPos + 12000;
            dumenofset = steeringVar.maMotorPos -direnctoPos;
        }
        else
        {
            float direnctoPos = (steeringVar.maDumenDirenc-190)*65;
            steeringVar.minlimit = steeringVar.maMotorPos -direnctoPos - 17000;
            steeringVar.maxlimit = steeringVar.maMotorPos -direnctoPos + 12000;
            dumenofset = steeringVar.maMotorPos -direnctoPos;

        }*/

       /* if(steeringVar.dumenofset*1.2 > dumenofset &&  steeringVar.dumenofset*0.8 < dumenofset)
        {

        }
        else
        {*/
            steeringVar.dumenofset = dumenofset;
        /*}*/

    }
    //    Serial.print(" , ");
    //    Serial.print(steeringVar.minlimit);
    //    Serial.print(" , ");
    //    Serial.print(steeringVar.maxlimit);
    //    Serial.print(" , ");
    //    Serial.print(steeringVar.dumenofset);
    //    Serial.print(" , ");


    /*  dumenPos.sensorAciDegeri = 0;

    dumenPos.calcPosValue = (dumenPos.sensorAciDegeri-1.65)*20000;

    dumenPos.maSensorAciDegeri = dumenPos.sensorAciDegeri*0.25 + dumenPos.maSensorAciDegeri*0.75;

    dumenPos.maCalcPosValue = (dumenPos.maSensorAciDegeri-1.65)*20000;
     */
}

void CalcFilteredPosition()
{
    static byte ptoggle = 0;

    if(ptoggle != steeringVar.toggleMesafe)
    {

        if(abs(steeringVar.mesafemm) < 20)
        {
            // Do nothing
        }
        else if(abs(steeringVar.mesafemm) < 50)
        {
            if(steeringVar.mesafemm >= 0)
            {
                dumenPos.filteredpos += 500;
            }
            else
            {
                dumenPos.filteredpos -= 500;
            }

        }
        else if(abs(steeringVar.mesafemm) < 1000)
        {
            if(steeringVar.mesafemm >= 0)
            {
                dumenPos.filteredpos += 700;
            }
            else
            {
                dumenPos.filteredpos -= 700;
            }

        }
        else
        {
            if(steeringVar.mesafemm >= 0)
            {
                dumenPos.filteredpos += 500;
            }
            else
            {
                dumenPos.filteredpos -= 500;
            }

        }
    }

}



void dmEnableDisableYonet()
{
    static byte prevPosEnable = 0;
    static unsigned int limitCnt = 0;
    //
    if(steeringVar.posEnableState == MANUEL)
    {
        if(motorControl ==1)
        {
            if(steeringVar.positionEnable == 1 && prevPosEnable == 0)
            {
                steeringVar.posEnableState = OTOMATIK;
                steeringVar.writenposEnableStatus = 1;
                steeringVar.writenPosValue = queryData.MotorPosition;
                manuelOtoGecisZS = millis();
#ifdef SteeringPRINT
                Serial.println("ElektrikDumeni Change OTOMATIK");
#endif
            }
            else
            {
                steeringVar.writenposEnableStatus = 0;

                if(limitCnt == 10)
                {
                    limitCnt = 0;
                }
                else if(limitCnt > 0)
                {
                    limitCnt++;
                }

                if(limitCnt == 0 &&
                (queryData.MotorPosition > 40000 || queryData.MotorPosition <-40000))
                {
                    steeringVar.writenPosValue = queryData.MotorPosition;
                    limitCnt = 1;
                }
            }
            steeringVar.writePosComplete = 0;
            prevPosEnable = steeringVar.positionEnable;
        }
    }
    else
    {
        limitCnt = 0;
        // OTOMATIK
        if(steeringVar.positionEnable == 0 || dmStatus.akimDurumu == 2)
        {
#ifdef SteeringPRINT
            Serial.println("ElektrikDumeni Change to MANUEL");
#endif
            steeringVar.posEnableState = MANUEL;
            steeringVar.writenposEnableStatus = 0;
        }
        else
        {
            steeringVar.writenposEnableStatus = 1;
        }
        prevPosEnable = steeringVar.positionEnable;
    }
}
void SorguSiralamasiniYonet()
{
    static long int posWriteZaman = 0;
    static byte ptoggleMesafe = 0;
    static int i = 0;
    static int position = 0;
    static int cnt = 0;
    static int inc = 0;
    static byte queryCnt = 0;
    if(i==0)
    {
        motorControl = 1;
#ifdef DUMENLEMETEST1
        steeringVar.positionMode = 1;
        steeringVar.speedMode=0;
        steeringVar.positionEnable = 1;
#endif
        if(millis() - manuelOtoGecisZS < 2000)
        {
            steeringVar.writenPosValue = 0;
        }
        else
        {
            if(steeringVar.writePosComplete == 1)
            {
#ifdef DUMENLEMETEST1
                steeringVar.position =     0; // position;
#endif
                float posValue = 0;

                Serial.print("toggleMesafe : ");
                Serial.print(steeringVar.toggleMesafe);
                if(ptoggleMesafe != steeringVar.toggleMesafe)
                {
                    if(steeringVar.position >0)
                    {
                        posValue = 500;
                    }
                    else if(steeringVar.position <0)
                    {
                        posValue = -500;
                    }
                    //posValue = steeringVar.position;
                    float nextPosValue = posValue+steeringVar.dumenofset;
                    //float posValue = steeringVar.position;
                    if(nextPosValue > steeringVar.maxlimit)
                    {
                        //posValue = steeringVar.maxlimit;
                        if(posValue >0)
                        {
                            posValue = 0;
                        }
                        else
                        {

                        }
                    }

                    if(nextPosValue < steeringVar.minlimit)
                    {
                        //posValue = steeringVar.minlimit;
                        if(posValue <0)
                        {
                            posValue = 0;
                        }
                        else
                        {

                        }

                    }
                    Serial.print("posValue : ");
                    Serial.print(posValue);
                    Serial.print(" , ");
                    Serial.print(steeringVar.maxlimit);
                    Serial.print(" , ");
                    Serial.print(steeringVar.minlimit);
                    Serial.print(" , ");
                    Serial.print(steeringVar.dumenofset);
                }
                if(steeringVar.writenposEnableStatus == 1)
                {
                    steeringVar.writenPosValue = posValue; // steeringVar.dumenofset - steeringVar.position;
                }
                else
                {
                    steeringVar.writenPosValue = 0;
                }
                //steeringVar.writenPosValue = dumenPos.filteredpos-dumenPos.offset+dumenPos.manueltoOtopos;

                ptoggleMesafe = steeringVar.toggleMesafe;
                //steeringVar.writenPosValue = queryData.MotorPosition;
            }
            else
            {
                steeringVar.writenPosValue = 0;
            }
        }


#ifdef DUMENLEMETEST1
        if(cnt == 10)
        {
            cnt = 0;
            if(inc ==1)
            {
                position = position + 10000;
                if(position > 40000)
                {
                    inc = 0;
                }

            }
            else
            {
                position = position - 10000;
                if(position < -40000)
                {
                    inc = 1;
                }
            }
        }
        else
        {

            cnt++;
        }
#endif
        i++;
    }
    else
    {
        static byte positionQuery = 0;
        motorControl = 0;
        if(positionQuery == 0)
        {
            steeringVar.queryMessage = 8;
            positionQuery = 1;
        }
        else
        {
            steeringVar.queryMessage = queryCnt;
            positionQuery = 0;
            queryCnt++;
            if(queryCnt == 10)
            {
                queryCnt = 0;
            }
        }
        i=0;
    }
}


void ElektrikDumeniIlklendir(void)
{

#ifdef SteeringPRINT
    Serial.println("ElektrikDumeniIlklendir");
#endif

#ifdef DUMENLEMETEST1
    steeringVar.posEnableState = OTOMATIK;
    steeringVar.writenposEnableStatus = 1;

#else
    steeringVar.posEnableState = MANUEL;
    steeringVar.writenposEnableStatus = 0;
#endif
    for (int c = 0; c < 10; c++)
    {
        steerZaman.queryZamanAsimi[c] = false;
        steerZaman.queryGecerlilik[c] = false;

    }
    steerZaman.posZamanAsimi = false;
}

void DumenlemeIceriginiDoldur(byte *veri)
{
    unsigned int ofset = 0;
    unsigned int usValue = 0;
    signed int sValue = 0;
    short shortValue = 0;
    unsigned short ushortValue = 0;
    byte byteValue = 0;

    // addr 4
    sValue = steeringVar.dumenDirenc; // 17.07 tarihinde degistirildi queryData.MotorPosition; // -8796; // Motor Position
    //sValue = queryData.MotorPosition; // -8796; // Motor Position
    memcpy(&veri[ofset],&sValue,sizeof(sValue));
    ofset +=sizeof(sValue);
    // addr8
    shortValue = queryData.electricalAngle; //  452; // Electrical Angle
    memcpy(&veri[ofset],&shortValue,sizeof(shortValue));
    ofset +=sizeof(shortValue);
    // addr10
    byteValue = queryData.MotorSpeed;// 60; // Speed
    memcpy(&veri[ofset],&byteValue,sizeof(byteValue));
    ofset +=sizeof(byteValue);
    // addr11
    byteValue = queryData.current; // Current
    memcpy(&veri[ofset],&byteValue,sizeof(byteValue));
    ofset +=sizeof(byteValue);
    // addr12
    ushortValue = queryData.mecRotorPosition; // Rotor Mech. Position
    memcpy(&veri[ofset],&ushortValue,sizeof(ushortValue));
    ofset +=sizeof(ushortValue);
    // addr14
    byteValue = queryData.controllerVoltage; // Voltage
    memcpy(&veri[ofset],&byteValue,sizeof(byteValue));
    ofset +=sizeof(byteValue);
    // addr15
    byteValue = (byte)queryData.temperature; // Temp
    memcpy(&veri[ofset],&byteValue,sizeof(byteValue));
    ofset +=sizeof(byteValue);
    // addr16
    ushortValue = queryData.ErrorCode.bayt; // Error Code
    memcpy(&veri[ofset],&ushortValue,sizeof(ushortValue));
    ofset +=sizeof(ushortValue);
}
