/*********************
 *Baglantilar
 **10 to GPS Module TX*
 **09 to GPS Module RX*
 *
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4 (for MKRZero SD: SDCARD_SS_PIN)
 **
 **
 **
 *********************/

#define LABTEST

#define ADS115MODULE
#define MOTORSURUCU
#define BNO055

#define BTCOMM
#define RASPCOMM
#define WIFI

#define PRINT(x) //Serial.print(x);
#define PRINTLN(x)// Serial.println(x);

#ifdef RASPCOMM
#define WRITERASP(x,y) Serial.write(x,y);
#define PRINTRASP(x) Serial.print(x);
#define PRINTLNRASP(x) Serial.println(x);
#endif

//#define byte char

#include <EEPROM.h>
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include <Wire.h>
#include <Adafruit_ADS1015.h>
#include <TinyGPS.h>
#include "BluetoothSerial.h" //Header File for Serial Bluetooth, will be added by default into Arduino

#include "tanimlamalar.h"
#ifdef BNO055
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#endif
#ifdef WIFI
#include "WiFi.h"
#include "AsyncUDP.h"
#include "WiFiUdp.h"
#include <ArduinoOTA.h>
TaskHandle_t Core1;
TaskHandle_t Core2;
//const char* ssid = "SUPERONLINE-WiFi_0318";
//const char* pass = "RMVHKY4LFNLT";
const char* ssid = "nirawifi";
const char* pass = "niranira";
AsyncUDP udp;
const int rele = 23;
#endif

extern void DumenlemeIceriginiDoldur(byte *veri);
extern void DumenlemeYonet(void);

void SwapFloatByteOrder(byte *array);

void CreateFile(void);
void gpsdump(TinyGPS &gps);
void printFloat(double f, int digits = 2);

void ADCDataParse(void);
void LedYonet(void);
void MotorYonet(void);
void SensorBno055DataParse(void);
void core1Yonet(void);
void CalcSteeringPID(void);
void MotorDondur(void);

void Core2Yonet(void);
void MotorDurumBilgisiOlustur();
int LED_BUILTIN = 2;
#ifdef MOTORSURUCU
int RPWM=26;
int LPWM=27;
// timer 0
int L_EN=33;
int R_EN=25;

const int ACISENSOR = 34;

const int preambleLength = 4;
const byte JOYSTICK_AXIS_X = 13;
const byte JOYSTICK_AXIS_Y = 5;
const byte motorEncoderB = 35; // kullanilmiyor kodda mevcut
const byte motorEncoderA = 18;

volatile unsigned int sensorCnt = 0;
volatile unsigned int sensorCnt2 = 0;
volatile unsigned int timecnt = 0;
unsigned int ptimeCnt = 0;
unsigned int yon = 0;

const unsigned int MOTORLOOPTIME = 100;
int maxIntegralValue = 20;

byte state = 0;
#endif

BluetoothSerial ESP_BT; //Object for Bluetooth
Adafruit_ADS1115 ads(0x48);

HardwareSerial mySerial(0);
HardwareSerial motorSerial(2);
TinyGPS gps;
int initilaze = 0;

File myFile;
File dosya;
//  char dosyaIsmi1[25] = "";
char dosyaIsmi1[30] = "logfile.txt";
char gpsGelenVeri[300] = "";
char gpsGelenVeriTemp[300] = "";
byte incVeri[22] = "";
byte motorVeri[30] = "";
byte bno055Veri[90] = "";
unsigned long dategps, timegps;

float voltage0 = 0.0;
float voltage1 = 0.0;
float voltage2 = 0.0;
float voltage3 = 0.0;
String prefix = "/gps";
String extension = ".txt";
String filename;
unsigned int fileNumber = 99999;


bool isGNGGAParse = false;
bool isGNVTGParse = false;
bool isGNRMCParse = false;
bool isGNTXTParse = false;
unsigned int gpsParseCnt = 0;


#ifndef ADS115MODULE
const int potPin = 34;
int potValue = 0;
const int potPinY = 35;
int potValueY = 0;
#else
#endif

struct CihazParseTipi
{
    bool gps;
    bool adc;
    bool bno055;
    bool motor;
}isCihazParse;

struct AutoSteerSettingsTipi{
    char ssid[24]      = "yourSSID";          // WiFi network Client name
    char password[24]  = "YourPassword";      // WiFi network password

    byte output_type = 2;       //set to 1  if you want to use Stering Motor + Cytron MD30C Driver
    //set to 2  if you want to use Stering Motor + IBT 2  Driver
    //set to 3  if you want to use IBT 2  Driver + PWM 2-Coil Valve
    //set to 4  if you want to use  IBT 2  Driver + Danfoss Valve PVE A/H/M

    byte input_type  = 2;       //0 = No ADS installed, Wheel Angle Sensor connected directly to ESP at GPIO 4 (attention 3,3V only)
    //1 = Single Mode of ADS1115 - Sensor Signal at A0 (ADS)
    //2 = Differential Mode - Connect Sensor GND to A1, Signal to A0

    byte IMU_type     = 0;      // set to 1 to enable BNO055 IMU

    byte Inclino_type = 0;      // set to 1 if MMA8452 is installed

    bool Invert_WAS   = 0;      // set to 1 to Change Direction of Wheel Angle Sensor - to +

    byte SWEncoder    = 0;      // Steering Wheel ENCODER Installed
    byte pulseCountMax= 3;      // Switch off Autosteer after x Pulses from Steering wheel encoder

    int SteerPosZero  = 512;

    byte SteerSwitchType = 2;   //0 = enable = switch high (3,3V) //1 = enable = switch low(GND) //2 = toggle = button to low(GND)
    //3 = enable = button to high (3,3V), disable = button to low (GND), neutral = 1,65V

    byte WorkSW_mode = 1;       // 0 = disabled   // 1 = digital ON/OFF // 2 = analog Value 0...4095 (0 - 3,3V)

    byte Invert_WorkSW = 0;     // 0 = Hitch raised -> High    // 1 = Hitch raised -> Low

    unsigned int WorkSW_Threshold = 1200;    // Value for analog hitch level to switch workswitch
    //##########################################################################################################
    //### End of Setup Zone ####################################################################################
    //##########################################################################################################
    float Ko = 0.05f;  //overall gain
    float Kp = 5.0f;  //proportional gain
    float Ki = 0.001f;//integral gain
    float Kd = 1.0f;  //derivative gain
    float steeringPositionZero = 13000;  byte minPWMValue=50;
    int maxIntegralValue=20;//max PWM value for integral PID component
    float steerSensorCounts=100;  int roll_corr = 0;
};
AutoSteerSettingsTipi autoSteerSettings;

struct AutoSteerYonetTipi
{
    float correction;
    float mesafemm;
    float angle;
    float steerAngleSetPoint;
    float steerAngleError;
    float pValue;
    float drive;
    int pwmDrive;
    byte rightpwm;
    byte leftpwm;
    int prevDrive;
    byte durum;


};
AutoSteerYonetTipi steer;
#ifdef BNO055
// Check I2C device address and correct line below (by default address is 0x29 or 0x28)
//                                   id, address
Adafruit_BNO055 bno = Adafruit_BNO055(55, 0x28);
/* Set the delay between fresh samples */
uint16_t BNO055_SAMPLERATE_DELAY_MS = 100;
#endif


#ifdef MOTORSURUCU
/*
void IRAM_ATTR mAinterrupt()
{
    sensorCnt++;
}

void IRAM_ATTR mBinterrupt()
{
    //Serial.print(sensorCnt);
    //PRINTLN(" RPM - ");
    //sensorCnt = 0;
    sensorCnt2++;
    int b = digitalRead(motorEncoderB);
    if(b == 1)
        yon = 0;
    else
        yon = 1;
}
*/
#endif

typedef enum core1Durum
{
    _CORE1_ILKLENDIRME = 0,
    _CORE1_YONET= 1
}core1DurumTipi;

struct altSistemVerisi
{
    bool ilklendirme = false;
    bool kullanim=false;
    bool ariza=true;
    bool gonderim = false;
};
struct SistemVerisi
{
    altSistemVerisi eeprom;
    altSistemVerisi gps;
    altSistemVerisi adc;
    altSistemVerisi bno055;
    altSistemVerisi motorSurucusu;
    altSistemVerisi sdCard;
    altSistemVerisi bt;
    bool veriGonder = false;
};
struct GpsVerisi
{
    bool yeniVeri = false;
    unsigned int cnt = 0;
    unsigned int tempCnt = 0;

};
GpsVerisi gpsStatus;
SistemVerisi sistem;
core1DurumTipi core1S = _CORE1_ILKLENDIRME;
SemaphoreHandle_t xMutex;

void setup()  
{
    /* create Mutex */
      xMutex = xSemaphoreCreateMutex();
    //------------------------------------------------------------------------------------------------------------
    //create a task that will be executed in the Core1code() function, with priority 1 and executed on core 0
    xTaskCreatePinnedToCore(Core1code, "Core1", 10000, NULL, 1, &Core1, 0);
    delay(500);
    //create a task that will be executed in the Core2code() function, with priority 1 and executed on core 1
    xTaskCreatePinnedToCore(Core2code, "Core2", 10000, NULL, 1, &Core2, 1);
    delay(500);
}
void loop() // run over and over
{

}
void EEPROMIlklendirme(void);
void GPSIlklendirme(void);
void ADCIlklendirme(void);
void BNO055Ilklendirme(void);
void MotorSurucuIlklendirme(void);
void BTIlklendirme(void);
void SDCardIlkelndirme(void);
void GPSYonet(void);
void BTVeriGonder(void);

void EEPROMIlklendirme()
{
    int addr = 0;
    unsigned int value = 0;

    EEPROM.begin(256);
    // Adres 0
    EEPROM.get(addr, value);
    delay(100);
    //addr += sizeof(value); // 2byte
    fileNumber = value;
    PRINTLN("value : ");
    PRINTLN(++value);
    EEPROM.put(addr, value);
    //addr += sizeof(value);
    delay(100);
    EEPROM.commit();
    delay(100);
    EEPROM.get(addr, value);
    PRINTLN("value : ");
    PRINTLN(value);
    sistem.eeprom.ilklendirme = true;
    sistem.eeprom.ariza = false;
}
void GPSIlklendirme()
{
    PRINTLN("");
    PRINT("Initializing GPS Serial Port...");
//    mySerial.begin(38400, SERIAL_8N1, 16, 17);
//    mySerial.begin(38400, SERIAL_8N1, 9, 10);
    mySerial.begin(38400, SERIAL_8N1, 3, 1);
    sistem.gps.ilklendirme = true;
    sistem.gps.ariza = false;
    delay(1000);
}
void ADCIlklendirme()
{
    PRINTLN("");
    PRINT("Initializing Analg Digital Converter...");
    ads.begin();
    sistem.adc.ilklendirme = true;
    sistem.adc.ariza = false;
}
void BNO055Ilklendirme()
{
#ifdef BNO055
    sistem.bno055.ilklendirme = false;
    sistem.bno055.ariza = true;
    /* Initialise the sensor */
    if(!bno.begin())
    {
        /* There was a problem detecting the BNO055 ... check your connections */
        PRINT("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    }
    else
    {
        sistem.bno055.ilklendirme = true;
        sistem.bno055.ariza = false;

    }
    delay(1000);
    //bno.setExtCrystalUse(true);
#endif
}
void MotorSurucuIlklendirme()
{


//     motorSerial.begin(115200, SERIAL_8N1, 9, 10);
    motorSerial.begin(115200, SERIAL_8N1, 16, 17);
#ifdef MOTORSURUCU

    autoSteerSettings.Ki = 0.15;
    autoSteerSettings.Kp = 5;
    autoSteerSettings.Ko = 0.05;
    autoSteerSettings.minPWMValue = 50;

 /*   pinMode(RPWM, OUTPUT);
    pinMode(LPWM, OUTPUT);
    pinMode(L_EN, OUTPUT);
    pinMode(R_EN, OUTPUT);

    // TODO pwm ler low yapilacak
    digitalWrite(RPWM, LOW);
    digitalWrite(LPWM, LOW);
    digitalWrite(L_EN, LOW);
    digitalWrite(R_EN, LOW);*/

 /*   ledcSetup(0, 500, 8);
    ledcSetup(1, 500, 8);*/
    // attach the channel to the GPIO to be controlled
 /*   ledcAttachPin(RPWM, 0);
 /   ledcAttachPin(LPWM, 1);*/


   /* ledcWrite(0,0);
    ledcWrite(1,0);*/
    /* DC Motor Encoder Sayici Interrupt*/
  /*  pinMode(motorEncoderA, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(motorEncoderA), mAinterrupt, CHANGE );*/
    /* Fan Sayici Interrupt*/
 /*   pinMode(motorEncoderB, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(motorEncoderB), mBinterrupt, CHANGE);*/


    sistem.motorSurucusu.ilklendirme = true;
    sistem.motorSurucusu.ariza = false;
#endif

    ElektrikDumeniIlklendir();
}
void BTIlklendirme()
{
    ESP_BT.begin("nira_odu123"); //Name of your Bluetooth Signal
    PRINTLN("Bluetooth Device is Ready to Pair");
    sistem.bt.ilklendirme = true;
    sistem.bt.ariza = false;
}
void SDCardIlkelndirme()
{
    PRINTLN("");
    PRINT("Initializing SD card...");

    CreateFile();
    sistem.sdCard.ilklendirme = true;
    sistem.sdCard.ariza = false;
}

void core1Ilklendirme(void)
{
    Serial.begin(38400);
    pinMode (LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);





    altsistemler.bit.gpsNTRIP = ENABLE;
    altsistemler.bit.Incilometer = ENABLE;
    altsistemler.bit.bno055 = ENABLE;
    altsistemler.bit.elektrikliDumen = ENABLE;
    altsistemler.bit.dumenAciOlcer = DISABLE;


    EEPROMIlklendirme();
    GPSIlklendirme();
    ADCIlklendirme();
    BNO055Ilklendirme();
    MotorSurucuIlklendirme();
    //BTIlklendirme();
    SDCardIlkelndirme();
}

void Core1code( void * pvParameters )
{
    for(;;)
    {
        if(core1S == _CORE1_ILKLENDIRME)
        {
            core1Ilklendirme();
            core1S = _CORE1_YONET;
        }
        else
        {
            core1Yonet();
        }
    }
}

void SeriVerileriKontrolEt(char c)
{
    /*Burada $GNGGA ve $GNVTG ve $GNRMC verileri parse edildi mi kontrol et*/
    //Serial.println("seri veri");
    static char p1,p2,p3,p4,p5;

    if(p5=='$' && p4=='G' && p3=='N')
    {
        if(p2 == 'G' && p1== 'G' && c=='A')
        {
            //Serial.println("seri veri gga ");
            isGNGGAParse = true;
        }
        if(p2 == 'V' && p1== 'T' && c=='G')
        {
            //Serial.println("seri veri vtg ");
            isGNVTGParse = true;
        }
        if(p2 == 'R' && p1== 'M' && c=='C')
        {
            //Serial.println("seri veri rmc ");
            isGNRMCParse = true;
        }
        if(p2 == 'T' && p1== 'X' && c=='T')
        {
            //Serial.println("seri veri rmc ");
            isGNTXTParse = true;
        }


        //Serial.println("seri veri check ");
    }


    byte b1 = c;
    byte b2 = p1;
    if((b1== 10) && (b2 == 13))
    {
        gpsParseCnt++;
        //Serial.println("seri veri COUNT ");
    }


    //Serial.println("seri veri end");
    p5=p4;
    p4=p3;
    p3=p2;
    p2=p1;
    p1=c;
}
void GPSYonet()
{


    memset(gpsGelenVeriTemp,0,sizeof(gpsGelenVeriTemp));

    //Serial.println(millis());
    if(mySerial.available()>10)
    {
        //Serial.println("serial port ");
        while(mySerial.available())
        {
            char c = mySerial.read();
            // TODO boyut kontrolu
            //PRINTLN("GPS Encode");
            if(gpsStatus.tempCnt <299)
            {
                gpsGelenVeriTemp[gpsStatus.tempCnt++] = c;
            }
            else
            {
                break;
            }
            isCihazParse.gps = true;
            sistem.gps.gonderim = true;
            SeriVerileriKontrolEt(c);

        }
    }

    //Serial.println("serial port 2");
   // Serial.println(millis());

    if(gpsStatus.tempCnt>0 && (gpsStatus.cnt+gpsStatus.tempCnt)<300 )
    {
        xSemaphoreTake( xMutex, portMAX_DELAY );
        memcpy(&gpsGelenVeri[gpsStatus.cnt],&gpsGelenVeriTemp[0],gpsStatus.tempCnt);

        xSemaphoreGive( xMutex );

#if 0
        for(int i = gpsStatus.cnt;i<(gpsStatus.cnt+gpsStatus.tempCnt);i++)
        {
            PRINT(gpsGelenVeri[i]);
        }

#endif
        gpsStatus.cnt += gpsStatus.tempCnt;
     //   PRINTLN(gpsStatus.cnt);
#if 1
        if(isGNGGAParse && isGNVTGParse && isGNRMCParse)
        {
            if(isGNTXTParse)
            {
                if(gpsParseCnt >=4)
                {
                    //Serial.println(gpsParseCnt);
                    //Serial.println("gpsParseCnt ");
                    gpsParseCnt = 0;
                    isGNGGAParse = false;
                    isGNVTGParse = false;
                    isGNRMCParse = false;
                    isGNTXTParse = false;
                    gpsStatus.yeniVeri = true;
                }

            }
            else
            {
                if(gpsParseCnt >=3)
                {
                   // Serial.println(gpsParseCnt);
                    //Serial.println("gpsParseCnt ");
                    gpsParseCnt = 0;
                    isGNGGAParse = false;
                    isGNVTGParse = false;
                    isGNRMCParse = false;
                    isGNTXTParse = false;
                    gpsStatus.yeniVeri = true;
                }
            }
        }

#endif
        gpsStatus.tempCnt = 0;

    }




}
void BTVeriGonder()
{
    byte serihat[300] = "";
    int result;
    if(isCihazParse.gps)
    {
        isCihazParse.gps = false;
        if(gpsStatus.cnt<300)
        {
            memcpy(&serihat[0],&gpsGelenVeri[0],gpsStatus.cnt);
#ifdef BTCOMM
            result = ESP_BT.write(&serihat[0],gpsStatus.cnt);
            ESP_BT.println();
#endif
#ifdef RASPCOMM
            WRITERASP(serihat,gpsStatus.cnt);
            Serial.write(10);
            //WRITERASP(10,1);
            //PRINTLNRASP("");
#endif
        }
    }
    if(isCihazParse.adc)
    {
        isCihazParse.adc = false;
#ifdef BTCOMM
        result = ESP_BT.write(&incVeri[0],22);
        ESP_BT.println();
#endif
#ifdef RASPCOMM
        WRITERASP(incVeri,22);
        Serial.write(10);
#endif
    }
    if(isCihazParse.bno055)
    {
        isCihazParse.bno055 = false;
#ifdef BTCOMM
        result = ESP_BT.write(&bno055Veri[0],83);
        ESP_BT.println();
#endif
#ifdef RASPCOMM
        WRITERASP(bno055Veri,83);
        Serial.write(10);
#endif
    }
    if(isCihazParse.motor)
    {
        isCihazParse.motor = false;
#ifdef BTCOMM
        result = ESP_BT.write(&motorVeri[0],20);
        ESP_BT.println();
#endif
#ifdef RASPCOMM
        WRITERASP(motorVeri,20);
        Serial.write(10);
        //WRITERASP(10,1);
        //PRINTLNRASP("");
#endif
    }
}

void core1Yonet(void)
{
    gpsStatus.yeniVeri = false;
    static unsigned long int zaman = 0;

    GPSYonet();

    LedYonet();
    MotorYonet();
    //Serial.println("core1Yonet");
    if (gpsStatus.yeniVeri || ((millis()-zaman)>500))
    {
        zaman = millis();
#ifdef ADS115MODULE
        //Serial.println(millis());
        ADCDataParse();
        //Serial.println(millis());
        delay(10);
#endif
        SensorBno055DataParse();
        //Serial.println(millis());
        gpsStatus.yeniVeri = false;
        //DosyayaYaz();
        //BTVeriGonder();
        sistem.veriGonder =true;
    }
    //Serial.println("LOOP");


    //Core2Yonet();

    delay(5);
}

void gpsdump(TinyGPS &gps)
{
    long lat, lon;
    float flat, flon;
    unsigned long age, chars;
    int year;
    byte month, day, hour, minute, second, hundredths;
    unsigned short sentences, failed;

    gps.get_position(&lat, &lon, &age);
    PRINT("Lat/Long(10^-5 deg): "); PRINT(lat); PRINT(", "); PRINT(lon);
    PRINT(" Fix age: "); PRINT(age); PRINTLN("ms.");

    // On Arduino, GPS characters may be lost during lengthy Serial.print()
    // On Teensy, Serial prints to USB, which has large output buffering and
    //   runs very fast, so it's not necessary to worry about missing 4800
    //   baud GPS characters.

    gps.f_get_position(&flat, &flon, &age);
    /* Serial.print("Lat/Long(float): "); printFloat(flat, 5); Serial.print(", "); printFloat(flon, 5);
    Serial.print(" Fix age: "); Serial.print(age); Serial.println("ms.");*/

    gps.get_datetime(&dategps, &timegps, &age);
    /*  Serial.print("Date(ddmmyy): "); Serial.print(dategps); Serial.print(" Time(hhmmsscc): ");
    Serial.print(timegps);
  Serial.print(" Fix age: "); Serial.print(age); Serial.println("ms.");*/

    gps.crack_datetime(&year, &month, &day, &hour, &minute, &second, &hundredths, &age);
    /* Serial.print("Date: "); Serial.print(static_cast<int>(month)); Serial.print("/");
    Serial.print(static_cast<int>(day)); Serial.print("/"); Serial.print(year);
  Serial.print("  Time: "); Serial.print(static_cast<int>(hour+8));  Serial.print(":"); //Serial.print("UTC +08:00 Malaysia");
    Serial.print(static_cast<int>(minute)); Serial.print(":"); Serial.print(static_cast<int>(second));
    Serial.print("."); Serial.print(static_cast<int>(hundredths)); Serial.print(" UTC +08:00 Malaysia");
  Serial.print("  Fix age: ");  Serial.print(age); Serial.println("ms.");*/

    /*  Serial.print("Alt(cm): "); Serial.print(gps.altitude()); Serial.print(" Course(10^-2 deg): ");
    Serial.print(gps.course()); Serial.print(" Speed(10^-2 knots): "); Serial.println(gps.speed());
  Serial.print("Alt(float): "); printFloat(gps.f_altitude()); Serial.print(" Course(float): ");
    printFloat(gps.f_course()); Serial.println();
  Serial.print("Speed(knots): "); printFloat(gps.f_speed_knots()); Serial.print(" (mph): ");
    printFloat(gps.f_speed_mph());
  Serial.print(" (mps): "); printFloat(gps.f_speed_mps()); Serial.print(" (kmph): ");
    printFloat(gps.f_speed_kmph()); Serial.println();*/

    gps.stats(&chars, &sentences, &failed);
    PRINT("Stats: characters: "); PRINT(chars); PRINT(" sentences: ");
    PRINT(sentences); PRINT(" failed checksum: "); PRINTLN(failed);
    /*if(initilaze == 1)
    {

        dosya = SD.open("/logfile.txt",FILE_APPEND);
        //dosya = SD.open(dosyaIsmi1);
        if (dosya)
        {
            dosya.print(dategps);
            dosya.print(",  ");
            dosya.print(timegps);
            dosya.print(",  ");
            dosya.print(lat);
            dosya.print(",  ");
            dosya.print(lon);
            dosya.print(",  ");
            dosya.print(gps.altitude());
            dosya.print(",  ");
            dosya.print(gps.f_speed_kmph());
            dosya.print(",  ");
            dosya.println("");
            PRINT("initlaze");

            // close the file:
            dosya.close();
        }
    }*/
}

void printFloat(double number, int digits)
{
    // Handle negative numbers
    if (number < 0.0)
    {
        PRINT('-');
        number = -number;
    }

    // Round correctly so that print(1.999, 2) prints as "2.00"
    double rounding = 0.5;
    for (uint8_t i=0; i<digits; ++i)
        rounding /= 10.0;

    number += rounding;

    // Extract the integer part of the number and print it
    unsigned long int_part = (unsigned long)number;
    double remainder = number - (double)int_part;
    PRINT(int_part);

    // Print the decimal point, but only if there are digits beyond
    if (digits > 0)
        PRINT(".");

    // Extract digits from the remainder one at a time
    while (digits-- > 0)
    {
        remainder *= 10.0;
        int toPrint = int(remainder);
        PRINT(toPrint);
        remainder -= toPrint;
    }
}
void SensorBno055DataParse()
{
#ifdef BNO055
    if(sistem.bno055.ilklendirme == true)
    {
        //could add VECTOR_ACCELEROMETER, VECTOR_MAGNETOMETER,VECTOR_GRAVITY...
        sensors_event_t orientationData , angVelocityData , linearAccelData, magnetometerData, accelerometerData, gravityData;
        delay(1);
        bno.getEvent(&orientationData, Adafruit_BNO055::VECTOR_EULER);
        delay(1);
        bno.getEvent(&angVelocityData, Adafruit_BNO055::VECTOR_GYROSCOPE);
        delay(1);
        bno.getEvent(&linearAccelData, Adafruit_BNO055::VECTOR_LINEARACCEL);
        delay(1);
        bno.getEvent(&magnetometerData, Adafruit_BNO055::VECTOR_MAGNETOMETER);
        delay(1);
        bno.getEvent(&accelerometerData, Adafruit_BNO055::VECTOR_ACCELEROMETER);
        delay(1);
        bno.getEvent(&gravityData, Adafruit_BNO055::VECTOR_GRAVITY);

        printEvent(&orientationData);
        printEvent(&angVelocityData);
        printEvent(&linearAccelData);
        printEvent(&magnetometerData);
        printEvent(&accelerometerData);
        printEvent(&gravityData);

        int8_t boardTemp = bno.getTemp();
        PRINTLN("");
        PRINT(F("temperature: "));
        PRINTLN(boardTemp);

        uint8_t system, gyro, accel, mag = 0;
        bno.getCalibration(&system, &gyro, &accel, &mag);
        PRINTLN("");
        PRINT("Calibration: Sys=");
        PRINT(system);
        PRINT(" Gyro=");
        PRINT(gyro);
        PRINT(" Accel=");
        PRINT(accel);
        PRINT(" Mag=");
        PRINTLN(mag);

        PRINT(" or-x=");
        PRINTLN(orientationData.orientation.x);
        PRINTLN("--");
        // Veri icerigini olustur;
        //Baslik $BO
        memset(bno055Veri,0,sizeof(bno055Veri));
        bno055Veri[0] = 36;
        bno055Veri[1] = 66;
        bno055Veri[2] = 79;
        bno055Veri[3] = 44;
        float value = 0;
        unsigned int ofset = 4;
        value = (float)orientationData.orientation.x;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)orientationData.orientation.y;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)orientationData.orientation.z;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)angVelocityData.gyro.x;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)angVelocityData.gyro.y;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)angVelocityData.gyro.z;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)linearAccelData.acceleration.x;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)linearAccelData.acceleration.y;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)linearAccelData.acceleration.z;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)magnetometerData.magnetic.x;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)magnetometerData.magnetic.y;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)magnetometerData.magnetic.z;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)accelerometerData.acceleration.x;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)accelerometerData.acceleration.y;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)accelerometerData.acceleration.z;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)gravityData.gyro.x;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)gravityData.gyro.y;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        value = (float)gravityData.gyro.z;
        memcpy(&bno055Veri[ofset],&value,sizeof(value));
        ofset +=4;
        memcpy(&bno055Veri[ofset],&boardTemp,sizeof(boardTemp));
        ofset +=1;
        memcpy(&bno055Veri[ofset],&system,sizeof(system));
        ofset +=1;
        memcpy(&bno055Veri[ofset],&gyro,sizeof(gyro));
        ofset +=1;
        memcpy(&bno055Veri[ofset],&accel,sizeof(accel));
        ofset +=1;
        memcpy(&bno055Veri[ofset],&mag,sizeof(mag));
        ofset +=1;
        bno055Veri[ofset]=42;
        byte checksum = CheckSumHesapla(&bno055Veri[0],ofset);
        ofset +=1;
        bno055Veri[ofset]=checksum; // TODO CheckSum Yazilacak
        ofset +=1;
        PRINTLN("Ofset :"); PRINT(ofset);

        isCihazParse.bno055 = true;
        sistem.bno055.gonderim = true;
    }
    else
    {
        //BNO055Ilklendirme();
    }

#endif
}

void printEvent(sensors_event_t* event)
{
    double x = -1000000, y = -1000000 , z = -1000000; //dumb values, easy to spot problem
    if (event->type == SENSOR_TYPE_ACCELEROMETER)
    {
        //Serial.print("Accl:");
        x = event->acceleration.x;
        y = event->acceleration.y;
        z = event->acceleration.z;
    }
    else if (event->type == SENSOR_TYPE_ORIENTATION)
    {
//        Serial.print("Orient:");
        x = event->orientation.x;
        y = event->orientation.y;
        z = event->orientation.z;

//        PRINT("\tx= ");
//        Serial.print(x);
//        Serial.print(" , ");
//        PRINT(" |\ty= ");
//        Serial.print(y);
//        Serial.print(" , ");
//        PRINT(" |\tz= ");
//        Serial.println(z);
    }
    else if (event->type == SENSOR_TYPE_MAGNETIC_FIELD)
    {
       // Serial.print("Mag:");
        x = event->magnetic.x;
        y = event->magnetic.y;
        z = event->magnetic.z;
    }
    else if (event->type == SENSOR_TYPE_GYROSCOPE)
    {
        //Serial.print("Gyro:");
        x = event->gyro.x;
        y = event->gyro.y;
        z = event->gyro.z;
    }
    else if (event->type == SENSOR_TYPE_ROTATION_VECTOR)
    {
       // Serial.print("Rot:");
        x = event->gyro.x;
        y = event->gyro.y;
        z = event->gyro.z;
    }
    else if (event->type == SENSOR_TYPE_LINEAR_ACCELERATION)
    {
        //Serial.print("Linear:");
        x = event->acceleration.x;
        y = event->acceleration.y;
        z = event->acceleration.z;
    }
    else
    {
        Serial.print("Unk:");
    }
//
//    PRINT("\tx= ");
//    Serial.print(x);
//    PRINT(" |\ty= ");
//    Serial.print(y);
//    PRINT(" |\tz= ");
//    Serial.println(z);
}

void LedYonet()
{

    static int led = 0;
    static unsigned long int zaman = 0;
    /*if((millis()-zaman)>1000)
    {
        zaman = millis();
        if(led == 0)
        {
            digitalWrite(LED_BUILTIN, HIGH);
            led = 1;
        }
        else
        {
            digitalWrite(LED_BUILTIN, LOW);
            led = 0;

        }
    }*/
}

void MotorYonet()
{

    static unsigned long int motorZaman = 0;
    static unsigned long int pMotorZaman = 0;
    static unsigned int loopZaman = 0;
#if 0
    if((millis()-motorZaman)>50)
    {
        motorZaman = millis();

        byte rpwm = 0;
        byte lpwm = 0;
        if(analogRead(JOYSTICK_AXIS_Y) > 600)
        {
            digitalWrite(R_EN,HIGH);
            digitalWrite(L_EN,HIGH);
            rpwm = 0;
            lpwm = 250;
            ledcWrite(0, rpwm);
            ledcWrite(1, lpwm);
        }
        else if(analogRead(JOYSTICK_AXIS_Y) <400)
        {
            digitalWrite(R_EN,HIGH);
            digitalWrite(L_EN,HIGH);
            rpwm = 250;
            lpwm = 0;
            ledcWrite(0, rpwm);
            ledcWrite(1, lpwm);
        }
        else
        {
            digitalWrite(R_EN,HIGH);
            digitalWrite(L_EN,HIGH);
            rpwm = 0;
            lpwm = 0;
            ledcWrite(0, rpwm);
            ledcWrite(1, lpwm);
        }
    }
#endif

    motorZaman = millis();
    if((motorZaman-pMotorZaman)>MOTORLOOPTIME)// &&
    //(sistem.motorSurucusu.gonderim == false))
    {
        delay(2);
        //int adc2 =
        int aciScale = analogRead(ACISENSOR);
        //Serial.println("");
        //Serial.print("ACISENSOR : ");
        //Serial.println(aciScale);
        voltage2 = ((aciScale * 3.3)/4096);
        if(voltage2 < 0.15 || voltage2 > 2.7)
        {

        }
        else
        {
            voltage2 = voltage2;
        }

        float direncVal =0;
        if(voltage2 !=8)
        {
            direncVal = (594*voltage2)/(8-voltage2);
        }
        else
        {
            direncVal = 190;
        }
       /* Serial.print(direncVal, 2);
        Serial.print(" aci=   , ");
        Serial.print(voltage2);
        Serial.println("    , ");*/
#ifdef LABTEST
        steeringVar.dumenDirenc = 190;

#else
        steeringVar.dumenDirenc = direncVal;
#endif
//        Serial.println("steer.durum : ");
//        Serial.println(steer.durum);
        pMotorZaman = motorZaman;
        // Burada 3 durum mevcut
        if(steer.mesafemm<=10 && steer.mesafemm>=-10)
        {
            //Herhangi Bir Duzeltme Yapma Cizgi uzerinde
            steer.durum = 0;
            steer.correction = 0;
            steer.steerAngleSetPoint = 0;
        }
        else if(steer.mesafemm<=25 && steer.mesafemm>=-25)
        {
            // Sinir Dahilinde ama Cizgiden Disari cikabilir

            steer.correction += autoSteerSettings.Ki;

            if(steer.correction>maxIntegralValue)
                steer.correction = maxIntegralValue;

            if(steer.mesafemm >0)
            {
                steer.steerAngleSetPoint -= steer.correction;
            }
            else
            {
                steer.steerAngleSetPoint += steer.correction;
            }
            if(steer.durum == 2)
            {
                steer.correction /= 8;
                steer.steerAngleSetPoint /= 8;
                /*if(steer.mesafemm >0)
                {
                    steer.steerAngleSetPoint -= steer.correction;
                }
                else
                {
                    steer.steerAngleSetPoint += steer.correction;
                }*/
            }
            steer.durum = 1;
        }
        else/* if(steer.mesafemm<45 && steer.mesafemm>-45)*/
        {
            // Sinir Disinda duzeltme yapilacak
            steer.correction += autoSteerSettings.Ki;

            if(steer.correction>maxIntegralValue)
                steer.correction = maxIntegralValue;

            if(steer.mesafemm >25)
            {
                steer.steerAngleSetPoint -= steer.correction;
            }
            else
            {
                steer.steerAngleSetPoint += steer.correction;
            }
            steer.durum = 2;
        }

        steer.steerAngleError = steer.steerAngleSetPoint;
        CalcSteeringPID();   //do the pid
        //MotorDondur();       //out to motors the pwm value
        /* Durum Bilgisi Olusturma*/
        xSemaphoreTake( xMutex, portMAX_DELAY );
        MotorDurumBilgisiOlustur();
        timecnt = 0;
        sensorCnt2 = 0;
        sensorCnt = 0;
        isCihazParse.motor = true;
        sistem.motorSurucusu.gonderim = true;
        xSemaphoreGive( xMutex );
        /*PRINT(sensorCnt2);
        PRINTLN(" RPM B - ");

        PRINT(sensorCnt);
        PRINTLN(" RPM A - ");
        PRINT(yon);
        PRINTLN(" yon");*/
        /*}
            timecnt = ptimeCnt;*/

        //Serial.println("Dumenleme");
        DumenlemeYonet();

    }// End of loop time

}

void MotorDurumBilgisiOlustur()
{
    // Veri icerigini olustur;
    //Baslik $BO MT
    motorVeri[0] = 36;
    motorVeri[1] = 77;
    motorVeri[2] = 79;
    motorVeri[3] = 44;
    unsigned int ofset = 4;
    unsigned int usValue = 0;
    signed int sValue = 0;
    short shortValue = 0;
    byte byteValue = 0;

#ifdef DCMOTOR

    // addr 4
    usValue = sensorCnt2;
    memcpy(&motorVeri[ofset],&usValue,sizeof(usValue));
    ofset +=sizeof(sValue);
    // addr8
    usValue = sensorCnt;
    memcpy(&motorVeri[ofset],&usValue,sizeof(usValue));
    ofset +=sizeof(sensorCnt);
    unsigned int zaman = millis()-loopZaman;
    memcpy(&motorVeri[ofset],&zaman,sizeof(zaman));
    ofset +=sizeof(zaman);
    loopZaman = millis();
    // Yazilan RPM Degeri Right
    memcpy(&motorVeri[ofset],&steer.rightpwm,sizeof(steer.rightpwm));
    ofset +=sizeof(steer.rightpwm);
    // Yazilan RPM Degeri Left
    memcpy(&motorVeri[ofset],&steer.leftpwm,sizeof(steer.leftpwm));
    ofset +=sizeof(steer.leftpwm);
    motorVeri[ofset] = 42;
    byte checksum = CheckSumHesapla(&motorVeri[0],ofset);
    ofset++;
    motorVeri[ofset] = checksum; // Checksum
#else

    DumenlemeIceriginiDoldur(&motorVeri[4]);


    ofset = 18;
    motorVeri[ofset] = 42;
    byte checksum = CheckSumHesapla(&motorVeri[0],ofset);
    ofset++;
    motorVeri[ofset] = checksum; // Checksum
#endif

}




byte
CheckSumHesapla(byte *veri, int boyut)
{
    byte checksum = 0;
    for (int i = 0;i<boyut;i++)
    {
        byte y = veri[i];
        if(i>=preambleLength && i<boyut)
        {
            checksum ^= y;
        }
    }

    return checksum;

}

void CalcSteeringPID(void)
{

    //Proportional
    steer.pValue = autoSteerSettings.Kp * steer.steerAngleError *autoSteerSettings.Ko;
    if(steer.pValue == 0 && steer.prevDrive != 0)
    {
        // Durdurma Mevcut
        steer.drive = steer.prevDrive/2;
    }
    else
    {
        steer.drive = steer.pValue;// + dValue;

    }

    steer.pwmDrive = (constrain(steer.drive, -255, 255));
    steer.prevDrive = steer.drive;


    //add throttle factor so no delay from motor resistance.
    if (steer.pwmDrive < 0 )
        steer.pwmDrive -= autoSteerSettings.minPWMValue;
    else if (steer.pwmDrive > 0 )
        steer.pwmDrive += autoSteerSettings.minPWMValue;

    if (steer.pwmDrive > 255)
        steer.pwmDrive = 255;
    if (steer.pwmDrive < -255)
        steer.pwmDrive = -255;

}

void MotorDondur(void)
{
    /*if (steerEnable == false)
        pwmDrive=0;
     */
    //pwmDisplay = pwmDrive;

    if (steer.pwmDrive > 0)
    {
        steer.rightpwm = steer.pwmDrive;
        steer.leftpwm = 0;

        digitalWrite(R_EN,HIGH);
        digitalWrite(L_EN,HIGH);
        ledcWrite(0, steer.pwmDrive);  // channel 0 = PWM_PIN
        ledcWrite(1, 0);
        //digitalWrite(DIR_PIN, LOW);
    }

    if (steer.pwmDrive <= 0)
    {
        steer.rightpwm = 0;
        steer.leftpwm = steer.pwmDrive;
        steer. pwmDrive = -1 * steer.pwmDrive;
        digitalWrite(R_EN,HIGH);
        digitalWrite(L_EN,HIGH);
        ledcWrite(1, steer.pwmDrive);  // channel 1 = DIR_PIN
        ledcWrite(0, 0);
        //digitalWrite(PWM_PIN, LOW);
    }
    //Serial.println(steer.pwmDrive);
}

void ADCDataParse()
{

#ifdef ADS115MODULE
    int adc0 = 0;
    int adc1 = 0;
    int adc2 = 0;
    int adc3 = 0;
    //adc0 = ads.readADC_SingleEnded(0);
    //delay(2);
    //adc1 = ads.readADC_SingleEnded(1);
//    Serial.print(adc0);
//    Serial.print(" , ");
//    Serial.print(adc1);
//    Serial.print(" , ");
    /*adc2 = ads.readADC_SingleEnded(2);
    adc3 = ads.readADC_SingleEnded(3);*/
//    adc0 += ads.readADC_SingleEnded(0);
//    delay(2);
//    adc1 += ads.readADC_SingleEnded(1);
//    Serial.print(adc0);
//    Serial.print(" , ");
//    Serial.print(adc1);
//    Serial.print(" , ");
 /*   adc2 += ads.readADC_SingleEnded(2);
    adc3 += ads.readADC_SingleEnded(3);*/
   /* adc0 += ads.readADC_SingleEnded(0);
    adc1 += ads.readADC_SingleEnded(1);
    adc2 += ads.readADC_SingleEnded(2);
    adc3 += ads.readADC_SingleEnded(3);
    adc0 += ads.readADC_SingleEnded(0);
    adc1 += ads.readADC_SingleEnded(1);
    adc2 += ads.readADC_SingleEnded(2);
    adc3 += ads.readADC_SingleEnded(3);*/
//    adc0 = adc0/2;
//    adc1 = adc1/2;
//    adc2 = adc2/2;
//    adc3 = adc3/2;

    int aciScale = analogRead(33);
    //Serial.print("1.Okuma :  ");
    //Serial.println(aciScale);
    voltage0 = ((aciScale * 3.3)/4096);
    aciScale = analogRead(32);
    //Serial.print("2.Okuma : ");
    //Serial.println(aciScale);
    voltage1 = ((aciScale * 3.3)/4096);

    //voltage0 = (adc0 * 0.1875)/1000;
    //voltage1 = (adc1 * 0.1875)/1000;
    voltage3 = (adc3 * 0.1875)/1000;


    //Baslik $AN
    incVeri[0] = 36;
    incVeri[1] = 65;
    incVeri[2] = 78;
    incVeri[3] = 44;
    byte intByteOrder[4] = {0};
    float angle = 0;
    angle = (voltage0 -0.5)*12.5-25;
   /* Serial.print(adc0);
    Serial.print(" , ");
    Serial.print(adc1);
    Serial.print(" , ");
    Serial.print(adc2);
    Serial.print(" , ");
    Serial.print(voltage0, 2);
    Serial.print(" , ");
    Serial.print(voltage1, 2);
    Serial.print("    , ");*/
//    Serial.print(direncVal, 2);
//    Serial.print("    , ");
    /*Serial.print(angle, 2);*/
    memcpy(&incVeri[4],&angle,sizeof(angle));
    angle = (voltage1 -0.5)*12.5-25;
   /* Serial.print(" , ");
    Serial.println(angle, 2);*/
    memcpy(&incVeri[8],&angle,sizeof(angle));
    memcpy(&incVeri[12],&adc2,sizeof(adc2));
    memcpy(&incVeri[16],&adc3,sizeof(adc3));
    incVeri[20] = 42;
    byte checksum = CheckSumHesapla(&incVeri[0],20);
    incVeri[21] = checksum; // Checksum


#ifdef PRINT
    PRINT("AIN0: ");
    PRINT(adc0);
    PRINT("\tVoltage: ");
    //Serial.println(voltage0, 7);
    PRINT("  AIN1: ");
    PRINT(adc1);
    PRINT("\tVoltage: ");
    //Serial.println(voltage1, 7);
    PRINTLN("");
#endif
#else
    potValue = analogRead(potPin);
    potValueY = analogRead(potPinY);
#ifdef PRINT
    PRINT(potValue);
    PRINT(" , ");
    PRINT(potValueY);
#endif

#endif
    isCihazParse.adc = true;
    sistem.adc.gonderim = true;
}

void DosyayaYaz()
{
    if(initilaze == 0)
    {
        String temp;
        temp = prefix;
        temp.concat(fileNumber);
        temp.concat(extension);
        filename = temp;

        File file = SD.open(filename, FILE_APPEND);
        if(!file)
        {
            PRINTLN("Failed to open file for writing");

        }
        else
        {
            initilaze = 1;
        }
        if(file.print("Log Basliyor"))
        {
            PRINTLN("File written");
            initilaze = 1;
        }
        else
        {
            PRINTLN("Write failed");
            initilaze = 0;
        }
    }
    else
    {
        dosya = SD.open(filename,FILE_APPEND);
        if (dosya)
        {
            PRINT("gpsGelenVeri");
            dosya.print("$AN");
            dosya.print(voltage0,7);
            dosya.print(":");
            dosya.print(voltage1,7);
            dosya.print(":");
            dosya.print(gpsGelenVeri);
            dosya.close();
        }
        else
        {
            PRINT("gpsGelenVeri else");
        }
    }
}

void CreateFile()
{

    if(!SD.begin()){
        PRINTLN("Card Mount Failed");
        return;
    }
    uint8_t cardType = SD.cardType();

    if(cardType == CARD_NONE){
        PRINTLN("No SD card attached");
        return;
    }

    PRINT("SD Card Type: ");
    if(cardType == CARD_MMC){
        PRINTLN("MMC");
    } else if(cardType == CARD_SD){
        PRINTLN("SDSC");
    } else if(cardType == CARD_SDHC){
        PRINTLN("SDHC");
    } else {
        PRINTLN("UNKNOWN");
    }

    uint64_t cardSize = SD.cardSize() / (1024 * 1024);
    //Serial.printf("SD Card Size: %lluMB\n", cardSize);

}

#define GELENVERILENGTH 100
#define GELENPREAMBLELENGTH 4

void CommIlklendirmeGiris(void);
void CommIklendirme(void);
void CommYonetGiris(void);
void CommYonet(void);
void Core2Yonet(void);
void GelenVeriParseEt(uint8_t *veri);
void WifiVeriGonder(void);

typedef enum commDurumVerisi
{
    _COMM_ILKLENDIRME_GIRIS= 0,
    _COMM_ILKLENDIRME= 1,
    _COMM_YONET_GIRIS = 2,
    _COMM_YONET = 3
}CommDurumTipi;

CommDurumTipi commDurum = _COMM_ILKLENDIRME_GIRIS;
unsigned long int core2zaman = 0;

// either use the ip address of the server or
// a network broadcast address
const char * udpAddress = "192.168.1.26";
const int udpPort = 1234;
static unsigned int wifiConnectCnt = 0;
static unsigned int wifiConnectResetCnt = 0;
//The udp library class
WiFiUDP udpwifi;
IPAddress wifiIpAddr;
uint8_t gelenBuffer[GELENVERILENGTH] = "hello world";

void Core2Yonet()
{
    switch (commDurum)
    {
        case _COMM_ILKLENDIRME_GIRIS:
            CommIlklendirmeGiris();
            break;
        case _COMM_ILKLENDIRME:
            CommIklendirme();
            break;
        case _COMM_YONET_GIRIS:
            CommYonetGiris();
            break;
        case _COMM_YONET:
            CommYonet();
            break;
        default:
            break;
    }
}
void CommIlklendirmeGiris()
{
    wifiConnectCnt = 0;
    WiFi.disconnect(true);
    WiFi.mode(WIFI_STA);
    commDurum = _COMM_ILKLENDIRME;
}
void CommIklendirme()
{
    WiFi.begin(ssid, pass);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        wifiConnectCnt++;
        if(wifiConnectCnt > 20) // 10 sn baglanri kuramazsa
        {
            wifiConnectResetCnt++;
            wifiConnectCnt = 0;
            commDurum = _COMM_ILKLENDIRME_GIRIS;
            //Serial.println("_COMM_ILKLENDIRME_GIRIS");
            //Serial.println(wifiConnectResetCnt);
            if(wifiConnectResetCnt >4)
            {
                if(ilkAcilisForwifi == 1)
                {
                    ilkAcilisForwifi = 0;
                    //ESP.restart();
                }
            }
            break;
        }
    }

    wifiConnectCnt = 0;
    if((WiFi.status() == WL_CONNECTED))
    {
        commDurum = _COMM_YONET_GIRIS;
    }
    else
    {
        commDurum = _COMM_ILKLENDIRME_GIRIS;

    }
}
void CommYonetGiris()
{
    if(otaClosed == 1)
    {
        ArduinoOTA
        .onStart([]() {
            String type;
            if (ArduinoOTA.getCommand() == U_FLASH)
                type = "sketch";
            else // U_SPIFFS
                type = "filesystem";

            // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
            Serial.println("Start updating " + type);
        })
        .onEnd([]() {
            Serial.println("\nEnd");
        })
        .onProgress([](unsigned int progress, unsigned int total) {
            Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
        })
        .onError([](ota_error_t error) {
            Serial.printf("Error[%u]: ", error);
            if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
            else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
            else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
            else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
            else if (error == OTA_END_ERROR) Serial.println("End Failed");
        });

        ArduinoOTA.begin();

        Serial.println("Ready");
        Serial.print("IP address: ");
        Serial.println(WiFi.localIP());
    }


    wifiConnectResetCnt = 0;
    //This initializes udp and transfer gelenBuffer
    udpwifi.begin(udpPort);
    core2zaman = millis();
    wifiIpAddr = WiFi.localIP();
    Serial.println(wifiIpAddr);
    wifiIpAddr[3] = 255;
    commDurum = _COMM_YONET;
}
void CommYonet()
{
    //Send a packet
    if(sistem.veriGonder)
    {
        Serial.println("veri Gonder");
        WifiVeriGonder();
        sistem.veriGonder = false;
    }
    memset(gelenBuffer, 0, sizeof(gelenBuffer));
    //processing incoming packet, must be called before reading the buffer
    udpwifi.parsePacket();
    //receive response from server, it will be HELLO WORLD
    if(udpwifi.read(gelenBuffer, GELENVERILENGTH) > 0)
    {
        core2zaman = millis();
//        Serial.print("Server to client: ");
//        Serial.println((char *)gelenBuffer);
        GelenVeriParseEt(gelenBuffer);
    }

    /* 3 sn'den daha uzun sure veri gelmezse tekrar baglnati kur*/
    if(millis() - core2zaman > 3000 )
    {
        commDurum = _COMM_YONET_GIRIS;
    }

    if(WiFi.status() != WL_CONNECTED)
    {
        /*Wifi Connection kayboldu tekrar kurulacak*/
        commDurum = _COMM_ILKLENDIRME_GIRIS;
    }

    if(otaClosed == 1)
    {
        ArduinoOTA.handle();
    }
    if(potaClosed == 0 && otaClosed == 1)
    {
        commDurum =_COMM_ILKLENDIRME_GIRIS;
        ilkAcilisForwifi = 1;
    }
    potaClosed = otaClosed;
    delay(20);

}
void WifiVeriGonder()
{
    udpwifi.beginPacket(wifiIpAddr,udpPort);
    if(sistem.gps.gonderim)
    {
        byte serihat[300] = "";

        xSemaphoreTake( xMutex, portMAX_DELAY );
#if 1
        memcpy(&serihat[0],&gpsGelenVeri[0],gpsStatus.cnt);
        sistem.gps.gonderim = false;
        udpwifi.write(serihat,gpsStatus.cnt);
        gpsStatus.cnt = 0;

#endif
        //memset(gpsGelenVeri,0,sizeof(gpsGelenVeri));
        xSemaphoreGive( xMutex );
    }
    if(sistem.bno055.gonderim)
    {
        udpwifi.write(bno055Veri, 83);
        sistem.bno055.gonderim = false;
    }
    if(sistem.adc.gonderim)
    {
        udpwifi.write(incVeri, 22);
        sistem.adc.gonderim = false;
    }
    if(sistem.motorSurucusu.gonderim)
    {
        xSemaphoreTake( xMutex, portMAX_DELAY );
        udpwifi.write(motorVeri,20);
        sistem.motorSurucusu.gonderim = false;
        xSemaphoreGive( xMutex );
    }
    udpwifi.endPacket();
}

void GelenVeriParseEt(uint8_t *veri)
{
    struct gelenVeriTipi
    {
        float angle;
        float correctionAngle;
        float mesafemm;
        byte surumCizgiUzerindemi;
        byte surumYontemi;
        byte uDonusOtomatik;
        byte uDonusDurumu;
        byte uDonusAciDegeri;
        byte electricalAutoSteeringMode;
        byte toggleMesafe;
        int electricalSteeringPosition;
        int electricalSteeringSpeed;



    };
    gelenVeriTipi gelenVeri;

    // Baslangic anahtar kelimesini kontrol et $TMD <36,84,77,68
    if(veri[0] == 36 && veri[1] == 84 && veri[2] == 77 && veri[3] == 68)
    {
       /* memcpy(&tohum_mitari, &veri[4], sizeof(tohum_mitari));
        memcpy(&rotor_bilgisi, &veri[8], sizeof(rotor_bilgisi));
        is_genisligi = veri[12];*/
        byte swapbyte[4] = {0};
        float floatValue = 0;
        int int32Value = 0;
        memcpy(&floatValue,&veri[4],sizeof(floatValue));
        gelenVeri.angle = floatValue;
//        Serial.println();
//        Serial.println(floatValue,3);
        memcpy(&floatValue,&veri[8],sizeof(floatValue));
        gelenVeri.mesafemm = floatValue;
//        Serial.println(floatValue,3);
        gelenVeri.surumCizgiUzerindemi = veri[12];
        if(veri[13] == 1)
        {
            gelenVeri.surumYontemi = 1;
        }
        else if(veri[13] == 2)
        {
            gelenVeri.surumYontemi = 0;
        }
        gelenVeri.uDonusOtomatik = veri[14];
        gelenVeri.uDonusDurumu = veri[15];
        gelenVeri.uDonusAciDegeri = veri[16];
        gelenVeri.electricalAutoSteeringMode = veri[17];
        gelenVeri.toggleMesafe = veri[19];

        memcpy(&swapbyte[0],&veri[20],4);
        SwapFloatByteOrder(&swapbyte[0]);
        memcpy(&int32Value,&swapbyte[0],sizeof(int32Value));
        gelenVeri.electricalSteeringPosition = int32Value;
        memcpy(&swapbyte[0],&veri[24],4);
        SwapFloatByteOrder(&swapbyte[0]);
        memcpy(&int32Value,&swapbyte[0],sizeof(int32Value));
        gelenVeri.electricalSteeringSpeed = int32Value;


//        Serial.println(veri[98]);
//        Serial.println(veri[99]);


        byte checksum = 0;
        for (int i = 0;i<GELENVERILENGTH;i++)
        {
            byte y = veri[i];
            if(i>=GELENPREAMBLELENGTH && i<(GELENVERILENGTH-2))
            {
                checksum ^= y;
            }
        }

        // Verinin son 2 bayt => "*CHECKSUM"
        if(veri[GELENVERILENGTH-2] == 42 && veri[GELENVERILENGTH-1] == checksum)
        {
           /*
            Serial.println("checksum");
            Serial.print(veri[98]); Serial.print(" ");
            Serial.print(veri[99]); Serial.print(" ");
            Serial.print(checksum); Serial.print(" ");

            Serial.println("a:");
            Serial.println(gelenVeri.surumYontemi);
            Serial.println("b:");
            Serial.println(gelenVeri.electricalAutoSteeringMode);
            Serial.println("c:");
            Serial.println(gelenVeri.electricalSteeringPosition);
            Serial.println("d:");


            Serial.print(veri[20]); Serial.print(" ");
            Serial.print(veri[21]); Serial.print(" ");
            Serial.print(veri[22]); Serial.print(" ");
            Serial.print(veri[23]); Serial.print(" ");
            */

            steer.mesafemm = gelenVeri.mesafemm;
            steer.angle = gelenVeri.angle;
            if(steeringVar.position != gelenVeri.electricalSteeringPosition)
            {
                Serial.println("electricalSteeringPosition: ");
                Serial.println(gelenVeri.electricalSteeringPosition);
            }
            if(steeringVar.speedEnable != (gelenVeri.electricalAutoSteeringMode & 0x01))
            {
                Serial.println("speedEnable: ");
                Serial.println(gelenVeri.electricalAutoSteeringMode & 0x01);
            }
            if(steeringVar.positionEnable != gelenVeri.surumYontemi)
            {
                Serial.println("positionEnable: ");
                Serial.println(gelenVeri.surumYontemi);
            }



            steeringVar.toggleMesafe = gelenVeri.toggleMesafe;
            steeringVar.speed = gelenVeri.electricalSteeringSpeed;
            steeringVar.mesafemm = gelenVeri.mesafemm;
            steeringVar.position = gelenVeri.electricalSteeringPosition;
            steeringVar.speedEnable = (gelenVeri.electricalAutoSteeringMode & 0x01);
//            steeringVar.positionEnable = (gelenVeri.electricalAutoSteeringMode & 0x02) >> 1;
            steeringVar.positionEnable =gelenVeri.surumYontemi;

            if(((gelenVeri.electricalAutoSteeringMode & 0x04) >> 2) == 1)
            {
                steeringVar.positionMode = 1;
                steeringVar.speedMode = 0;
            }
            else
            {
                steeringVar.positionMode = 0;
                steeringVar.speedMode = 1;
            }


        }
        else
        {
            Serial.println("Checksum Hatasi");
        }
    }
    else
    {
        Serial.println("Anahtar Kelime Yanlis");
    }
}

void Core2code( void * pvParameters )
{
    Serial.println();
    Serial.print("Task2 running on core ");
    Serial.println(xPortGetCoreID());

    for(;;)
    {
        Core2Yonet();
    }


    /*
    WiFi.disconnect(true);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, pass);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    //This initializes udp and transfer buffer
    udpwifi.begin(udpPort);
    for(;;)
    {*/
    /*if (udp.listen(1234)) {
            Serial.print("UDP Listening on IP: ");
            Serial.println(WiFi.localIP());
            udp.onPacket([](AsyncUDPPacket packet) {
                Serial.print("UDP Packet Type: ");
                Serial.print(packet.isBroadcast() ? "Broadcast" : packet.isMulticast() ? "Multicast" : "Unicast");
                Serial.print(", From: ");
                Serial.print(packet.remoteIP());
                Serial.print(":");
                Serial.print(packet.remotePort());
                Serial.print(", To: ");
                Serial.print(packet.localIP());
                Serial.print(":");
                Serial.print(packet.localPort());
                Serial.print(", Length: ");
                Serial.print(packet.length()); //dlzka packetu
                Serial.print(", Data: ");
                Serial.write(packet.data(), packet.length());
                Serial.println();
                String myString = (const char*)packet.data();
                if (myString == "ZAP") {
                    Serial.println("Zapinam rele");
                    digitalWrite(rele, LOW);
                } else if (myString == "VYP") {
                    Serial.println("Vypinam rele");
                    digitalWrite(rele, HIGH);
                }
                packet.printf("Got %u bytes of data", packet.length());
            });*/
    //udp.print("hello server");
    /*      delay(200);
            uint8_t buffer[50] = "hello world";
            //Send a packet
            udpwifi.beginPacket(udpAddress,udpPort);
            udpwifi.write(incVeri, 25);
            udpwifi.endPacket();
            memset(buffer, 0, 50);
            //processing incoming packet, must be called before reading the buffer
            udpwifi.parsePacket();
            //receive response from server, it will be HELLO WORLD
            if(udpwifi.read(buffer, 50) > 0){
              Serial.print("Server to client: ");
              Serial.println((char *)buffer);
            }
            //Wait for 1 second
            delay(1000);*/
    //}
}


void
SwapFloatByteOrder(byte *array)
{
    byte tempArray[4] = {0};

    tempArray[3] = array[0];
    tempArray[2] = array[1];
    tempArray[1] = array[2];
    tempArray[0] = array[3];

    memcpy(&array[0],&tempArray[0],4);
//    array[0] = tempArray[0];
//    array[1] = tempArray[1];
//    array[2] = tempArray[1];
//    array[3] = tempArray[2];


}
